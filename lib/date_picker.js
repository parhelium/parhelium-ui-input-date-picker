var logger = loggerFactory('date_picker')

Template.DatePicker.helpers({})

Template.DatePicker.events({})

Template.DatePicker.created = function(){}

Template.DatePicker.rendered = function(){
    var self = this;
    var vars = this.view.parentView.dataVar.curValue;
    this.autorun(function(){
        logger.log( 'DatePicker.autorun -> ', vars.options );
        var $input  = self.$('.date_picker').pickadate(vars.options);
        self.picker = $input.pickadate('picker')
        self.picker.on({
            open: function() {
                var x = ( $(window).width()  -450 ) / 2;
                var y = ( $(window).height() - 250 ) / 2

                self.picker.$root.css( 'position', 'fixed' ) ;
                self.picker.$root.css( 'left', x+'px' ) ;
                self.picker.$root.css( 'top', y+'px' ) ;
            },
            render: function() {
            },
            close : function(){

            }
        })
        self.picker.open();

    })

}

Template.DatePicker.destroyed = function(){}