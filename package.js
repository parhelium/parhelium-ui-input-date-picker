Package.describe({
  name: 'parhelium:ui-input-date-picker',
  version: '0.0.2',
  summary: 'Added Date-Picker using pickadate',
  git: '',
  documentation: 'README.md'
});

client = ['client'];
both   = ['client', 'server'];

Package.onUse(function(api) {
  api.versionsFrom("METEOR@1.0");

  api.use( [
    'templating',
    'parhelium:logger',
    'cwohlman:pickadate'
  ], client );

  api.addFiles( [
    'lib/date_picker.html',
    'lib/date_picker.js'
  ], client );
});